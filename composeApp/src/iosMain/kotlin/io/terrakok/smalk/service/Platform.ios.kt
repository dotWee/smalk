package io.terrakok.smalk.service

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.russhwolf.settings.NSUserDefaultsSettings
import com.russhwolf.settings.Settings
import io.github.aakira.napier.DebugAntilog
import kotlinx.datetime.Instant
import net.folivo.trixnity.client.media.MediaStore
import net.folivo.trixnity.client.media.okio.OkioMediaStore
import net.folivo.trixnity.client.store.repository.realm.createRealmRepositoriesModule
import okio.Path
import okio.Path.Companion.toPath
import org.jetbrains.skia.Image
import org.koin.core.module.Module
import platform.Foundation.*

actual fun getPlatformSettings(): Settings = NSUserDefaultsSettings(
    NSUserDefaults("SmalkSettings")
)

actual fun ByteArray.asImageBitmap(): ImageBitmap =
    Image.makeFromEncoded(this).toComposeImageBitmap()

actual fun getLogger(defaultTag: String): DebugAntilog = DebugAntilog(defaultTag)

actual fun createDateFormat(pattern: String): (instant: Instant) -> String =
    object : (Instant) -> String {
        private val formatter = NSDateFormatter().apply {
            setDateFormat(pattern)
        }

        override fun invoke(instant: Instant) = formatter.stringFromDate(
            NSDate.dateWithTimeIntervalSince1970(instant.epochSeconds.toDouble())
        )
    }

private fun getCacheDirectoryPath(): Path {
    val cacheDir = NSSearchPathForDirectoriesInDomains(
        NSCachesDirectory,
        NSUserDomainMask,
        true
    ).first() as String
    return "$cacheDir/cache".toPath()
}

actual suspend fun getPlatformRepositoryModule(): Module = createRealmRepositoriesModule {
    directory(getCacheDirectoryPath().resolve("realm").toString())
}

actual suspend fun getPlatformMediaStore(): MediaStore = OkioMediaStore(
    getCacheDirectoryPath().resolve("media")
)