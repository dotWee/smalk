package io.terrakok.smalk.service

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.russhwolf.settings.Settings
import com.russhwolf.settings.StorageSettings
import io.github.aakira.napier.DebugAntilog
import kotlinx.datetime.Instant
import net.folivo.trixnity.client.media.MediaStore
import net.folivo.trixnity.client.media.indexeddb.IndexedDBMediaStore
import net.folivo.trixnity.client.store.repository.indexeddb.createIndexedDBRepositoriesModule
import org.jetbrains.skia.Image
import org.koin.core.module.Module
import kotlin.js.Date

actual fun getPlatformSettings(): Settings = StorageSettings()

actual fun ByteArray.asImageBitmap(): ImageBitmap =
    Image.makeFromEncoded(this).toComposeImageBitmap()

actual fun getLogger(defaultTag: String): DebugAntilog =
    DebugAntilog(defaultTag)

actual fun createDateFormat(pattern: String): (Instant) -> String {
    //JS pattern is different from standard
    //JVM: https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/text/SimpleDateFormat.html
    //JS: https://github.com/felixge/node-dateformat/blob/master/Readme.md#mask-options
    val mask = pattern.map { c ->
        when (c) {
            'm' -> 'M'
            'M' -> 'm'
            else -> c
        }
    }.joinToString("")
    return { jsFormatDate(Date(it.toEpochMilliseconds()), mask) }
}

actual suspend fun getPlatformRepositoryModule(): Module = createIndexedDBRepositoriesModule("smalk")
actual suspend fun getPlatformMediaStore(): MediaStore = IndexedDBMediaStore("smalk")